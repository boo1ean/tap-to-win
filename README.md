## API Endpoints

All Requests are signed with access token (passed through headers)

```
"tap-token": "4m32iv423ivmrwevrvwer4"
```

### POST /contacts

Push contacts list to the server and get "app authorized" contact info

```json
req:

[
	"380994358899",
	"380955438002",
	"380913943433"
]

res:

[
	{
		"phone": "092323994",
		"score": 3434,
	},
	{
		"phone": "092323994",
		"score": 3434,
	},
	{
		"phone": "092323994",
		"score": 3434,
	}
]
```

Backend: trying to fetch given phone number from database, if found -> get scores for found number
Associate nubmer to the user

### GET /auth/request

Request for control string

```json
res:

{
	"control-string": "miqmerioqmewirqmviwe"
}
```

### POST /auth/sign

Sign auth and get access token


```json
req:

{
	"device_id": "qei3nuu2v",
	"sign": "12cj12cm891cej9w8j8wjrew8jwecr8mewjrchew8crh32c234c23"
}


res: 

{
	"token": "qn23u4vh238n4ht7347vt489m3q4cnq34cgt"
}
```
